#!/usr/bin/python

from prime import prime
import operator
import math

def euler009():
    max = 0
    for i in range(1,1000):
       for j in range(1,1000):
           k = math.sqrt(i*i + j*j)
	   if k == int(k) and (i + j + k) == 1000:
	      print i , j , k
	      print i*i , j*j , k*k
              return i*j*k

if __name__=='__main__':
   print euler009()
