#!/usr/bin/python

from prime import prime, is_prime
import operator
import math



def euler035():
    res = set()

    p = []
    for n in prime():

        if n > 1000000:
          break  
        print n
        p.append(n)

    pset = set(p)

    for n in p:

        def is_circular(n):
            s = str(n)
            rotations = [ int(s[d:] + s[:d]) for d in range(len(s))]
            return all(map(lambda x: x in pset ,rotations)) 

        if is_circular(n):
           res.add(n)
           print n 

    return len(res)

if __name__=='__main__':
    print euler035()
