#!/usr/bin/python

def euler005():
    dividers = range(1,21)
    res = 20
    while 1:
       res = res + 1
       for d in dividers:
           if res % d != 0:
	      break
       else:
           return res

if __name__=='__main__':
    print euler005()
