#!/usr/bin/python

from prime import prime
import operator
import math

def euler010():
    max = 0
    for i in prime():
       if i > 2000000:
          return max
       max = max + i

if __name__=='__main__':
   print euler010()
