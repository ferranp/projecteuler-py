#!/usr/bin/python

from prime import prime
import operator
import math
import itertools

data = open("triangle.txt").read().strip()

def euler018():
    #print [ line.split(" ") for line in data.split("\n") ]
    triangle = [ map(int,line.split(" ")) for line in data.split("\n") ]
    tmax=[]
    for lin in triangle:
        tmax.append( [0] * len(lin) )
    
    i = len(triangle) - 1
    tmax[i] = triangle[i][:]

    for lin in reversed(range(len(triangle) - 1)):
        for col in range(lin + 1):
            tmax[lin][col] = max((tmax[lin+1][col],tmax[lin+1][col+1])) + triangle[lin][col]

    #print "\n".join( map(str,tmax) )   

    return tmax[0][0]

if __name__=='__main__':
   print euler018()

