#!/usr/bin/python

from divisors import divisors,divisors_slow
from prime import factorize
import operator
import math


def euler012():
    i = 1
    tri = 1
    while 1:
      if divisors(tri) > 500:
         print factorize(tri)
         print i, tri , divisors(tri)
	 d = divisors_slow(tri) 
	 print d,len(d)
	 if len(d) > 500:
             return tri
      if i % 200 == 0:  print i, tri , divisors(tri)
      i = i + 1	
      tri = tri + i

if __name__=='__main__':
   print euler012()
