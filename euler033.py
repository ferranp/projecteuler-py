#!/usr/bin/python

from prime import prime, is_prime
import operator
import math


def euler033():
    res = 0
    
    digits = '123456789'
    # a / b = a1 /b1
    nums = []
    for a in range(10,100):
        for b in range(a+1,100):
            c = float(a) / float(b)
            astr = str(a)
            bstr = str(b)
            as0,as1 = astr[0],astr[1]
            bs0,bs1 = bstr[0],bstr[1]
            if as0 not in bstr and as1 not in bstr:
                continue
            if as1 == '0' and bs1 =='0':
                continue
            if as0 == bs0 and float(bs1) > 0:
               c1 = float(as1) / float(bs1)
               if c1 == c:
                  #print "%d / %d = %f" % (a,b,c)
                  print "%s / %s = %f" % (as1,bs1,c)
                  nums.append((as1,bs1))
            if as0 == bs1 and float(bs0) > 0:
               c1 = float(as1) / float(bs0)
               if c1 == c:
                  #print "%d / %d = %f" % (a,b,c)
                  print "%s / %s = %f" % (as1,bs0,c)
                  nums.append((as1,bs0))
            if as1 == bs0 and float(bs1) > 0:
               c1 = float(as0) / float(bs1)
               if c1 == c:
                  #print "%d / %d = %f" % (a,b,c)
                  print "%s / %s = %f" % (as0,bs1,c)
                  nums.append((as0,bs1))
            if as1 == bs1 and float(bs0) > 0:
               c1 = float(as0) / float(bs0)
               if c1 == c:
                  #print "%d / %d = %f" % (a,b,c)
                  print "%s / %s = %f" % (as0,bs0,c)
                  nums.append((as0,bs0))
    print nums
    a = reduce(operator.mul, [int(n[0]) for n in nums],1) 
    b = reduce(operator.mul, [int(n[1]) for n in nums],1) 
    res = b / a     
    return res

if __name__=='__main__':
   print euler033()
