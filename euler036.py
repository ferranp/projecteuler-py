#!/usr/bin/python

from prime import prime, is_prime
import operator
import math

def is_palindromic(s):
    sp= "".join([x for x in reversed(s)])    
    return  s == sp 

def int2bin(n):
    bstr = ''
    if n < 0: raise ValueError, "must be a positive integer"
    if n == 0: return '0'
    while n > 0:
        bstr = str(n % 2) + bstr
        n = n >> 1
    return bstr

def euler036():
    res = []

    for n in range(1000000):
        if is_palindromic(str(n)):
            nbin = int2bin(n) 
            if is_palindromic(nbin):           
                print n , nbin       
                res.append(n)

    return sum(res)

if __name__=='__main__':
    print euler036()

