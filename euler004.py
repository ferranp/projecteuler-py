#!/usr/bin/python

from prime import prime
import operator

def euler004():
    max = 0
    for n in range(100,999):
      for m in range(n,999):
        num = n * m 
        s = str(num)
	s1 = ''.join(c for c in reversed(s))
	if  s == s1:
	    if num > max:
	        max = num
    return max

if __name__=='__main__':
   print euler004()
