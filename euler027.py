#!/usr/bin/python

from prime import prime, is_prime
import operator
import math

def quadratic(a,b):
    def f(n):
        return n*n + a*n + b 
    return f

def euler027():
    max = 0
    res = 0
    for a in range(-1000,1000):
        for b in prime():
            if b > 1000:
               break
            count = 0
            f = quadratic(a,b)
            n = 1
            while is_prime(f(n)):
               n = n + 1
            if n > max:
               print a,b,n
               max = n
               res = a*b     
    return res

if __name__=='__main__':
   print euler027()
