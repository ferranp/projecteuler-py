#!/usr/bin/python

from prime import prime
import operator
import math

names = {
1:'one',
2:'two',
3:'three',
4:'four',
5:'five',
6:'six',
7:'seven',
8:'eight',
9:'nine',
10:'ten',
11:'eleven',
12:'twelve',
13:'thirteen',
14:'fourteen',
15:'fifteen',
16:'sixteen',
17:'seventeen',
18:'eighteen',
19:'nineteen',
20:'twenty',
30:'thirty',
40:'forty',
50:'fifty',
60:'sixty',
70:'seventy',
80:'eighty',
90:'ninety',
100:'hundred',
1000:'thousand',
}

def name_of(num):
    res = ""
    
    if num in names:
       if num == 100 or num==1000:
          res="one"
       res = res+names[num]
       print res
       return res   

    uni = num % 10
    dec = (num - uni) /10
    cen = 0

    if dec > 9:
       cen = dec / 10
       dec = dec - cen*10 

    if cen > 0:
       res = res + names[cen] + names[100]
       if dec !=0 or uni != 0:
          res=res+'and'

    if dec < 2:
       if (dec*10 + uni) != 0:
          res = res + names[dec*10 + uni]
    else:
       res = res + names[dec*10] 
       if uni > 0:
          res =res +names[uni]
    print res
    return res

def len_of(num):
    print num
    return len(name_of(num))

def euler017():
    res=0
    for i in range(1,1001):
        res = res +  len_of(i)

    return res

if __name__=='__main__':
   print euler017()
