#!/usr/bin/python

from prime import prime
import operator
import math
import itertools
import datetime

def fact(num):
    if num == 0:
       return 1
    return num *  fact(num - 1)


def euler020():

    res = 0
    
    num = fact(100)
    print num
    for i in str(num):
       res = res + int(i)

    return res

if __name__=='__main__':
   print euler020()

