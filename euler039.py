#!/usr/bin/python

from prime import prime, is_prime
import operator
import math




def euler039():
    res = 0
    rmax = 0
    results = {}

    for p in range(1000):
        results[p] = []
        for c1 in range(p/2):
            for c2 in range(c1,p/2):
                c3 = math.sqrt(c1*c1 + c2*c2)                    
                per = c1 + c2 + c3
                if per == p:
                    results[p].append((c1,c2,c2))
    
    for k,v in results.iteritems():
        if len(v) > rmax:
            print k,v
            rmax = len(v)
            res = k
    return res
if __name__=='__main__':
    print euler039()

