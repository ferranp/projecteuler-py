#!/usr/bin/python

from prime import prime
import operator
import math


def euler016():
    res=0
   
    pows = [[1]]
    for ii in range(1,1001):
        nou= [ x*2 for x in pows[ii-1] ]	
        if nou[0] > 9:
	   nou.insert(0,1)
	   nou[1] = nou[1] - 10
        for i,v in enumerate(nou):
	    if v > 9:
	       nou[i] = nou[i] - 10
               nou[i - 1 ] = nou[i-1] + 1       
	#print ii , nou
	pows.append(nou)
    
    for x in pows[-1]:
        res =  res + x
    return res

if __name__=='__main__':
   print euler016()
