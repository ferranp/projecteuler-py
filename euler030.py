#!/usr/bin/python

from prime import prime, is_prime
import operator
import math




def euler030():
    res = 0
    MAX = pow(9,5)

    digits = 1
    
    while MAX*digits > int('9'*digits):
         digits+=1

    print digits , MAX*digits

    pows = dict([ (str(n),pow(n,5)) for n in range(10) ])

    for i in range(2,MAX*digits):
        num = sum([ pows[digit] for digit in str(i) ])
        if num == i:
           print i
           res+=i 
  
    return res

if __name__=='__main__':
   print euler030()
