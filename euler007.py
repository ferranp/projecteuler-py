#!/usr/bin/python

from prime import prime
import operator

def euler007():
    for i,p in enumerate(prime()):
      if i == 10000:
         return p

if __name__=='__main__':
   print euler007()
