#!/usr/bin/python

from prime import prime, is_prime
import operator
import math


def euler032():
    res = 0
    
    digits = '123456789'
    # a * b = c
    nums = []
    for a in range(10000):
        sa = str(a)
        if '0' not in sa:
            for b in range(a):
                sb = str(b)
                #if len(sa+sb) > 5:
                #   break
                if '0' not in sb:
                    c = a*b
                    sc = str(c)
                    if len(sa+sb+sc) > 9:
                        break
                    if len(sa+sb+sc) == 9:
                        s = sa+sb+sc                        
                        if all( [digit in s for digit in digits ] ):
                            if c not in nums:
                               nums.append(c)                            
                            print "%d x %d = %d" % (a,b,c)
    res = sum(nums)    
    return res

if __name__=='__main__':
   print euler032()
