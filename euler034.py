#!/usr/bin/python

from prime import prime, is_prime
import operator
import math

def fact(n):
    if n < 2:
       return 1
    return n * fact(n-1)

def euler034():
    res = 0
    facts = dict( [(str(n),fact(n)) for n in range(10)] )
    print facts 
    MAX = fact(9)    

    digits =1
    while int('9'*digits) < MAX*digits:
       digits +=1
             
    print digits

    MAX = int('9'*digits) + 1
    for n in range(3,MAX):
        num = sum([facts[d] for d in str(n) ])
        if num == n:
           print num
           res +=num 

    return res

if __name__=='__main__':
   print euler034()
