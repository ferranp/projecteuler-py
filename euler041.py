#!/usr/bin/python

from prime import prime, is_prime
import operator
import math

import itertools

def is_pandigital(num):
    snum = str(num)
    for d in range(1,len(snum)+1):
        if str(d) not in snum:
            return False
    return True

def euler041():
    res = 1
    for n in prime():
        if len(str(n)) > 9:
            break
        if is_pandigital(n):
            res = n
            print n
    return res
if __name__=='__main__':
    print euler041()

