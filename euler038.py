#!/usr/bin/python

from prime import prime, is_prime
import operator
import math

def concat(num,n):
    return "".join()    

digits = ('1','2','3','4','5','6','7','8','9')

def euler038():
    res = ""

    for num in xrange(1000000):
        c = ""
        for n in range(1,10):
            c = c + str(num * n)
            if len(c) == 9 and n > 1:
                for d in digits:
                    if d not in c:
                        break
                else:
                    if res < c:
                        res = c                 
                    print "%d,%d -> %s" %(num,n,c)
    return res
if __name__=='__main__':
    print euler038()

