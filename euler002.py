#!/usr/bin/python

from fibonacci import fib

def euler002():
    res = 0
    for i in fib():
       if i > 4000000:
          break
       if (i % 2 != 0):
           res = res + i
    return res

if __name__=='__main__':
   print euler002()
