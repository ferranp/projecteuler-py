#!/usr/bin/python

from fibonacci import fib
import operator
import math
import itertools
import datetime

def euler025():
    for i,x in enumerate(fib()):
        if len(str(x)) == 1000:
           print x
           return i + 1

if __name__=='__main__':
   print euler025()

