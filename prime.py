#!/usr/bin/python

import math

_primes = [2,3]
_primes_set = set([2,3])

def prime():
    for n in _primes:
        yield n
    n = _primes[-1]
    while True:
       n = n + 2
       maxi = math.sqrt(n)
       for d in _primes:
            if d > maxi:
                yield n 
                _primes.append(n)
                _primes_set.add(n)
                break
            if n %d == 0:
	            break

def is_prime(num):
    if num < 1:
       return False
    if num == 1:
       return False
    if _primes[-1] > num:
       return num in _primes_set
    for n in prime():
       if n == num:
            return True 
       if n > num:
            return False 
          

def factorize(num):
    max = math.sqrt(num)
    factors = []
    for p in prime():
        while num % p == 0:
	   factors.append(p)
           num = num / p 
	if num == 1:
	    break

    return factors

if __name__ == '__main__':
   for n in prime():
      if n > 1000:
         break
      print n	 
