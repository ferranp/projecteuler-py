#!/usr/bin/python

from prime import prime,factorize
import operator
import math
import itertools
import datetime


def divisors(num):
    res = []
    i = 1
    while i <= ( num/2 ): 
        if num % i == 0: 
           res.append(i)
	i = i+1
    return res


def euler023():
    res = 0
    abundant = []

    for i in range(28123):
        x = sum(divisors(i))
        if x > i:
           abundant.append(i)

    nums = [False] * 28123 

    for i in abundant:
        for j in abundant:
            k = i + j
            if k < 28123: 
               nums[k] = True
    for n,x in enumerate(nums):
       if not x:
          print n
          res = res + n    
    return res

if __name__=='__main__':
   print euler023()

