#!/usr/bin/python

def fib():
    n2 = 1
    n1 = 1
    while True:
       n0 = n1 + n2
       yield n2
       n2 = n1 
       n1 = n0


if __name__ == '__main__':
   for n in fib():
      if n > 1000:
         break
      print n	 
