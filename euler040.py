#!/usr/bin/python

from prime import prime, is_prime
import operator
import math

import itertools


def digits():
    for n in itertools.count():
        for c in str(n):
            yield c


def euler040():
    res = 1
    for c,n in enumerate(digits()):
        if c in (1,10,100,1000,10000,100000,1000000):
            print n
            res = res * int(n)
        if c == 1000000:
            break

    return res
if __name__=='__main__':
    print euler040()

