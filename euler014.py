#!/usr/bin/python

from prime import prime
import operator
import math

def seq(n):
  while n > 1:
    if n % 2 == 0:
       n = n / 2
    else:
       n = (3 * n) + 1
    yield n
  return  


def euler014():
    numbers = [ 0 for x in range(1000000) ]
    for i in range(1000000):
        count = 0
        for n in seq(i):
	    count = count + 1
	    if n < 1000000:
	       if numbers[n] > 0:
	          count = numbers[n] + count 
                  break 
	numbers[i] = count

    max = (0,0)
    for i,val in enumerate(numbers):
        if val > max[1]:
	   max = (i,val)

    return max

if __name__=='__main__':
   print euler014()
