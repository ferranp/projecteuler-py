#!/usr/bin/python

from prime import prime, is_prime
import operator
import math


def getnext(MAX):
    lin0,col0 = ((MAX - 1) / 2,(MAX - 1)/2)
    for radius in range(0, (MAX + 1) / 2 ):
      col = radius
      lin = 0
      for lin in range(-radius + 1,radius):
        yield lin0+lin,col0 + col
      lin = lin + 1
      for col in range(radius,-radius,-1):
        yield lin0+lin,col0 + col
      col = col - 1
      for lin in range(radius,-radius,-1):
        yield lin0+lin,col0 + col
      lin = lin - 1
      for col in range(-radius,radius+1,1):
        yield lin0+lin,col0 + col


def euler028():
    res = 0
    MAX = 1001
    #matrix = [ [ 0 ] * MAX for x in range(MAX) ]

    #vals = range(1,MAX*MAX + 1)
    i=0
    for lin,col in getnext(MAX):
        #val = vals.pop(0)
        i+=1
        if lin == col or lin == (MAX - col -1):
           res+=i
           print lin,col,res
         
        #matrix[lin][col] = val    

    #for x in matrix:
    #    print " ".join(map(lambda x: "%3d" % x,x))

    return res

if __name__=='__main__':
   print euler028()
