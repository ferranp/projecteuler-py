#!/usr/bin/python

from prime import prime, is_prime
import operator
import math




def euler029():
    nums = set()

    for a in range(2,101):
       for b in range(2,101):
           n = math.pow(a,b)
           if n not in nums:
              nums.add(n)

    return len(nums)

if __name__=='__main__':
   print euler029()
