#!/usr/bin/python

from prime import prime
import operator
import math

calc = {}

def paths(x,y):

    if x == 20:
       return 1
    
    if y == 20:
       return 1

    if (x,y) not in calc:
       calc[(x,y)] = paths(x,y+1) + paths(x+1,y)
    
    return calc[(x,y)]

def euler015():
    return paths(0,0)


if __name__=='__main__':
   print euler015()
