#!/usr/bin/python

from prime import prime, is_prime
import operator
import math


def conv(curr,num,coins):
    coin = coins[0]
    new_coins = coins[1:]
    if coin == 1:
        new_curr = curr[:]
        new_curr.append(num)
        yield new_curr
    else:     
        for n in range(divmod(num,coin)[0]+1):
            new_curr = curr[:]
            new_curr.append(n)
            new_num = num - (n * coin)
            for x in conv(new_curr,new_num,new_coins):
                yield x

def euler031():
    res = 0
    
    coins = [1, 2, 5, 10, 20, 50, 100,200]
    coins.reverse()
    for x in conv([],200,coins):
        print x
        res +=1 
    return res

if __name__=='__main__':
   print euler031()
