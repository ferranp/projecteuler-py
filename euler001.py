#!/usr/bin/python

def euler001():
    res = 0
    for i in range(1000):
       if (i % 3 == 0) or (i % 5 == 0):
           res = res + i
    return res

if __name__=='__main__':
   print euler001()
