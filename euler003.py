#!/usr/bin/python

from prime import prime
import operator

def euler003():
    num = 600851475143
    factors = []
    for p in prime():
        while num % p == 0:
	   factors.append(p)
           num = num / p 
	if num == 1:
	    break

    print factors
    assert 600851475143 == reduce(operator.mul,factors,1) 
    return max(factors)

if __name__=='__main__':
   print euler003()
