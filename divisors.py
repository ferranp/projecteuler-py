#!/usr/bin/python

import math
from prime import prime,factorize
import operator

def fact(x): 
    if x==0:
       return 1
    return x * fact(x-1)

def divisors(num):
    res = 0
    factors = factorize(num)
    n = len(factors)
    num = 0
    for p in range(1,n):
       num = num + (fact(n) / (fact(n - p) * fact(p)))
        
    return num + 2

def divisors_slow(num):
    res = []
    i = 1
    while i <= ( num/2 ): 
        if num % i == 0: 
           res.append(i)
	i = i+1
    res.append(num)	
    return res

if __name__ == '__main__':
   print divisors(120)
