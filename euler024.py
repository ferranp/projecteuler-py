#!/usr/bin/python

from prime import prime,factorize
import operator
import math
import itertools
import datetime

MAX=10
NUMS = range(MAX)

def perm(num,pos,excl):
    #print "perm(%s,%s,%s)" % (num,pos,excl)
    if pos == (MAX - 1):
       for nn in NUMS:
           if nn not in excl:
              num[pos] = nn
              yield num
    else:      
       for n in NUMS:
           if n not in excl:
              num[pos] = n 
              _excl = excl[:]
              _excl.append(n) 
              for x in perm(num,pos+1,_excl):
                 yield x
 
def euler024():
    res = 0
    count = 0
    num = [0] * MAX
    for x in perm(num,0,[]):
        count =  count + 1
        if count == 1000000:
           print x
           return ''.join(map(str,x))

if __name__=='__main__':
   print euler024()

