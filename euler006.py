#!/usr/bin/python

def euler006():
    res = sum(range(101))
    res = res * res
    for i in range(101):
       res = res - i*i 
    return res

if __name__=='__main__':
   print euler006()
