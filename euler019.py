#!/usr/bin/python

from prime import prime
import operator
import math
import itertools
import datetime


def euler019():
    
    delta7 = datetime.timedelta(days=7)
    date = datetime.date(1901,1,1)
        
    count = 0
    # first monday
    wd = date.weekday()
    if wd != 6:
       date = date + datetime.timedelta(days=6-wd  )
    print count,date
    while date.year < 2001:
       if date.day == 1:
          count = count + 1
          print count,date,date.weekday()
       date = date + delta7
    
    return count

if __name__=='__main__':
   print euler019()

