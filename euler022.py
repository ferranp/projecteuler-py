#!/usr/bin/python

from prime import prime,factorize
import operator
import math
import itertools
import datetime

VAL = ord("A") - 1

def score(name):
    s = 0
    for x in name:
       s = s + (ord(x) - VAL) 
    return s   

def euler022():
    print score("COLIN")
    res = 0
    names = [x.strip('"') for x in open('names.txt').read().split(',') ]
    names.sort()
    for i,n in enumerate(names):
        res = res + (i+1)*score(n)
    return res

if __name__=='__main__':
   print euler022()

