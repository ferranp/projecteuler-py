#!/usr/bin/python

from prime import prime, is_prime
import operator
import math

def euler037():
    res = []
    digits = 8

    p = { }

    for d in range(2,digits):
        print "digits ",d
        maxn= 10**d
        for n in prime():
            if n < 10**(d-1):
                continue
            if n > maxn:
                break
            ok = True
            sn = str(n)
            for k in range(1,len(sn)):     
                n1 = int(sn[:k])
                n2 = int(sn[k:])
                if not is_prime(n1):
                    ok = False
                    break
                elif not is_prime(n2):
                    ok = False
                    break
            if ok:  
                res.append(n)
                print n                
                if len(res) == 11:
                    print res
                    return sum(res)                    

if __name__=='__main__':
    print euler037()

