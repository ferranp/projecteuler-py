#!/usr/bin/python

from fibonacci import fib
import operator
from decimal import *
import itertools
import datetime
import fpformat

c = getcontext()
c.prec = 5000
setcontext(c)

def euler026():
    max = 1
    res = 0
    for i in range(1,1000):
         n = Decimal(1) / Decimal(i) 
         s = str(n) 
         if len(s) > 300:
           s = s[10:]
           for l in range(1,len(s)/2):
              #print ">>" , s[0:l] , "|", s[l:l + l]
              if s[0:l] == s[l:l+l] and s[0:l] == s[l+l:l+l+l]:
                 #print i,l
                 if l > max:
                    max = l
                    res = i
                    print res,max,str(n)
                 break
    
    return res         

if __name__=='__main__':
   print euler026()

