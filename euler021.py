#!/usr/bin/python

from prime import prime,factorize
import operator
import math
import itertools
import datetime


def divisors(num):
    res = []
    i = 1
    while i <= ( num/2 ): 
        if num % i == 0: 
           res.append(i)
	i = i+1
    return res

def get_d(num):
    return sum(divisors(num))


def euler021():
    res = 0
    d = []
    for i in range(10000):
        d.append(get_d(i))
    
    for i in range(10000):
        x = d[i]
      
        if x != i and x < 10000 and x > 1 and  d[x] == i:
           print i
           res = res + i

    return res

if __name__=='__main__':
   print euler021()

